#! /bin/sh

xsetroot -solid black
xset -dpms 
xset s off
xset b off

setxkbmap -model pc105 -layout us,gr -option grp:alt_shift_toggle

xrandr --output DVI-D-0 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-0 --mode 1920x1080 --pos 1920x0 --rotate normal --output DP-0 --off --output DP-1 --off --output DVI-D-1 --off

while true; do

    VOL=$(amixer get Master | awk -F'[]%[]' '/%/ {if ($7 == "off") { print "MM" } else { print $2/10 }}' | head -n 1)
    DF=$(date +"%a %d %b %Y %H:%M")
    KBD=$(getxkblayout)

    xsetroot -name "[${KBD}]  [Vol: ${VOL}]  [${DF}]"
    sleep 1s

done &
